﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class NumberToImageCollection{

    public List<NumberToImage> entries = new List<NumberToImage>();

    public void SetCollection(int number){

        for (int i = 0; i < entries.Count; i++){
            
            entries[i].SetImageToSprite(number);
        }
    }
}
