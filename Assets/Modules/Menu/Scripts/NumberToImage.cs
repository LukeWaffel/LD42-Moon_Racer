﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class NumberToImage : MonoBehaviour{

	public int indexFromLast;
	public int onlyEnableIfNumberAbove;
	public int expectedDigits;

	public List<Sprite> sprites = new List<Sprite>();

	public Image image;

	public void SetImageToSprite(int number){
		
		if (number < onlyEnableIfNumberAbove){
			image.sprite = sprites[0];
			return;
		}
		
		string numberString = number.ToString();

		List<string> splitNumber = new List<string>();

		for (int i = 0; i < numberString.Length; i++){
			
			splitNumber.Add(numberString[i].ToString());
		}
		
		int toAdd = expectedDigits - splitNumber.Count;

		for (int i = 0; i < toAdd; i++){
			
			splitNumber.Insert(0,"0");
		}
		
		image.sprite = sprites[Convert.ToInt32(splitNumber[splitNumber.Count-indexFromLast-1])];
	}
}
