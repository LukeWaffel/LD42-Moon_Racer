﻿using System.ComponentModel;
using UnityEngine;
using UnityEngine.UI;

public class UFOController : MonoBehaviour{

	[Header("Wheel references")] 
	public WheelJoint2D frontWheel;
	public WheelJoint2D backWheel;
	public WheelCollisionChecker frontCollision;
	public WheelCollisionChecker backCollision;

	private Rigidbody2D rigid;
	private JointMotor2D frontMotor;
	private JointMotor2D backMotor;
	
	[Header("Speed config")]
	public float drivingSpeed = 5000f;
	public float airControlSpeed = -500f;
	public float airControlModifier;

	[Header("Misc")]
	public bool inControl = true;
	
	// Use this for initialization
	void Start (){

		rigid = GetComponent<Rigidbody2D>();
		
		frontMotor = frontWheel.motor;
		backMotor = backWheel.motor;
	}
	
	// Update is called once per frame
	void Update (){

		if (!inControl){

			frontWheel.useMotor = false;
			backWheel.useMotor = false;
			
			return;
		}

		SetAirControlModifier();
		
		float input = Input.GetAxis("Horizontal");

		rigid.angularVelocity += (airControlSpeed * airControlModifier * input) * Time.deltaTime;
		
		if(frontCollision.isGrounded)
			frontMotor.motorSpeed = input * drivingSpeed * -1f;
		else
			frontMotor.motorSpeed = 0;

		if (backCollision.isGrounded)
			backMotor.motorSpeed = input * drivingSpeed * -1f;
		else
			backMotor.motorSpeed = 0;

		frontWheel.motor = frontMotor;
		backWheel.motor = backMotor;
		
		if (input != 0 || Input.GetKey(KeyCode.Space)){

			frontWheel.useMotor = true;
			backWheel.useMotor = true;
		}
		else{

			frontWheel.useMotor = false;
			backWheel.useMotor = false;
		}
	}

	public void ToggleControls(bool state){

		inControl = state;
	}

	void SetAirControlModifier(){
		
		if (frontCollision.isGrounded && backCollision.isGrounded){
		
			airControlModifier = 0;
		}else if (frontCollision.isGrounded || backCollision.isGrounded){

			airControlModifier = 0.5f;
		}else{

			airControlModifier = 1;
		}
	}
}
