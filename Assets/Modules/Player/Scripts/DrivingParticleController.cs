﻿using UnityEngine;

public class DrivingParticleController : MonoBehaviour{

	[Header("References")]
	public Rigidbody2D ufoRigid;
	public ParticleSystem particleSystem;
	public WheelCollisionChecker collsionChecker;
	public Transform target;

	[Header("Base config")]
	public Vector3 offset;

	[Header("Particle Config")]
	public float baseEmissionRate;
	public float baseSpeed;


	// Use this for initialization
	void Start (){

		particleSystem = GetComponent<ParticleSystem>();
		collsionChecker = transform.parent.GetComponent<WheelCollisionChecker>();
		
		target = transform.parent;
		ufoRigid = transform.root.GetComponentInChildren<Rigidbody2D>();
		transform.SetParent(transform.root);
		
		//Setting up particle stuff
		baseEmissionRate = particleSystem.emission.rateOverTimeMultiplier;
		baseSpeed = particleSystem.main.startSpeedMultiplier;
	}
	
	// Update is called once per frame
	void Update (){

		transform.position = target.transform.position + offset;

		ParticleSystem.EmissionModule emission = particleSystem.emission;
		emission.rateOverTimeMultiplier = collsionChecker.isGrounded ? baseEmissionRate * ufoRigid.velocity.x : 0;

		ParticleSystem.MainModule main = particleSystem.main;
		main.startSpeed = baseSpeed * ufoRigid.velocity.x;
	}

	[ContextMenu("Set Offset")]
	void SetOffset(){

		offset = transform.localPosition;
	}
}
