﻿using UnityEngine;
using UnityEngine.UI;

public class FuelManager : MonoBehaviour{

	public Image fuelBar;
	public UFOController controller;
	public AudioSource fuelPickupSound;
	
	public float fuel = 100;
	public float fuelDegrationValue = 1;

	void Update (){

		fuel -= fuelDegrationValue * Time.deltaTime;

		fuelBar.fillAmount = fuel / 100;
		
		controller.ToggleControls(fuel > 0);
	}

	private void OnTriggerEnter2D(Collider2D other){

		if (other.CompareTag("Fuel")){

			fuel = 100;
			
			fuelPickupSound.Play();
			
			Destroy(other.gameObject);
		}		
	}
}
