﻿using UnityEngine;

public class SpringController : MonoBehaviour{

	public Transform targetA;
	public Transform targetB;
	
	// Update is called once per frame
	void Update (){

		Vector3 midPoint = (targetA.position + targetB.position) / 2;
		
		transform.position = midPoint;

		Vector3 direction = targetA.position - targetB.position;
		
		transform.rotation = Quaternion.LookRotation(Vector3.forward, direction.normalized);
		
		Debug.DrawRay(transform.position, direction, Color.yellow);
		
	}
}
