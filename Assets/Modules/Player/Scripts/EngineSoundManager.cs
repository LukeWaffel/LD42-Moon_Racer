﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EngineSoundManager : MonoBehaviour{

	public AudioSource engineAudioSource;
	public Rigidbody2D carRigidBody;

	public float factor = 10;

	public AnimationCurve pitchOverVelocity;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update (){


		engineAudioSource.pitch = pitchOverVelocity.Evaluate(carRigidBody.velocity.x);
	}

	public void StopAudio(){
		
		engineAudioSource.Stop();
	}
}
