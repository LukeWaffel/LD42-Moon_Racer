﻿using UnityEngine;

public class WheelCollisionChecker : MonoBehaviour{

	[Header("References")]
	public CircleCollider2D collider;
	public GameObject UFO;
	
	[Header("Config")]
	public float margin = .1f;
	public LayerMask castMask;
	
	[Header("Result")]
	public bool isGrounded;
	
	// Use this for initialization
	void Start (){

		collider = GetComponent<CircleCollider2D>();
		UFO = GameObject.Find("UFO Body");
	}
	
	// Update is called once per frame
	void Update (){

		RaycastHit2D normalHit = Physics2D.Raycast(transform.position, -UFO.transform.up, collider.radius + margin, castMask.value);
		RaycastHit2D downHit = Physics2D.Raycast(transform.position, Vector3.down, collider.radius + margin, castMask.value); 

		isGrounded = normalHit.collider != null || downHit.collider != null;
		
		Debug.DrawLine(transform.position, (transform.position + -UFO.transform.up * (collider.radius + margin)), normalHit.collider ? Color.green : Color.red);
		Debug.DrawLine(transform.position, (transform.position + Vector3.down * (collider.radius + margin)), downHit.collider ? Color.green : Color.red);
	}
}
