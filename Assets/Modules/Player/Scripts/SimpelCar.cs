﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpelCar : MonoBehaviour{

	public WheelJoint2D frontWheel;
	public WheelJoint2D backWheel;
	
	public JointMotor2D frontMotor;
	public JointMotor2D backMotor;

	public float speed = 1000;

	public bool isGrounded;

	public LayerMask castMask;

	public Rigidbody2D rigid;
	public float rotSpeed = 10;
	
	// Use this for initialization
	void Start (){

		rigid = GetComponent<Rigidbody2D>();
		
		frontMotor = frontWheel.motor;
		backMotor = backWheel.motor;

	}
	
	// Update is called once per frame
	void Update (){

		isGrounded = IsGrounded();

		if (!IsGrounded()){

			rigid.angularVelocity += Input.GetAxisRaw("Horizontal") * rotSpeed * Time.deltaTime;
		}
		
		float speed = Input.GetAxis("Horizontal") * this.speed * -1;

		frontMotor.motorSpeed = speed;
		backMotor.motorSpeed = speed;
		
		frontWheel.motor = frontMotor;
		backWheel.motor = backMotor;
		
		if (Input.GetAxis("Horizontal") != 0 || Input.GetKey(KeyCode.Space)){
			
			frontWheel.useMotor = true;
			backWheel.useMotor = true;
		}else
		{
			
			frontWheel.useMotor = false;
            backWheel.useMotor = false;
		}

		if (Input.GetKeyDown(KeyCode.R)){
			
			transform.position = new Vector3(transform.position.x, transform.position.y + 5, transform.position.z);
			transform.eulerAngles = Vector3.zero;

			GetComponent<Rigidbody2D>().angularVelocity = 0;
			GetComponent<Rigidbody2D>().velocity = Vector2.zero;

			frontWheel.connectedBody.angularVelocity = 0;
			backWheel.connectedBody.angularVelocity = 0;

			backWheel.connectedBody.velocity = Vector2.zero;
			frontWheel.connectedBody.velocity = Vector2.zero;
		}
	}

	private bool IsGrounded(){
		
		RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down, 1.25f, castMask.value);

		Vector3 point = new Vector3(transform.position.x, transform.position.y - 1.25f, transform.position.z);
			
		Debug.DrawLine(transform.position, point, hit.collider == null ? Color.red : Color.green);
		
		return hit.collider != null;
	}
}
