﻿using UnityEngine;

public class GameManager : MonoBehaviour{
	
	public GameObject player;
	public GameObject gameScorePanel;
	public GameObject endPanel;

	public ScoreManager scoreTracker;
	public UFOController ufoController;
	public EngineSoundManager engineSound;
	public FuelManager fuelManager;
	
	public float crashTime = 5;
	public float fuelTime = 2.5f;
	public float deadTimer;
	public bool updateTimer;
	
	public enum DeathCause {Crashed, OutOfFuel}
	public DeathCause deathCause;
	
	// Update is called once per frame
	void Update (){
		
		UpdateTimer();
		
		if (player.transform.eulerAngles.z < 218 && player.transform.eulerAngles.z > 135){

			if (updateTimer)
				return;
			
			deadTimer = crashTime;
			updateTimer = true;
			deathCause = DeathCause.Crashed;
		}
		else{
			
			if(deathCause == DeathCause.Crashed)
				updateTimer = false;
		}

		if (fuelManager.fuel <= 0){

			if (updateTimer)
				return;

			deadTimer = fuelTime;
			updateTimer = true;
			deathCause = DeathCause.OutOfFuel;
		}
		else{

			if (deathCause == DeathCause.OutOfFuel)
				updateTimer = false;
		}
	}

	private void UpdateTimer(){

		if (!updateTimer)
			return;
		
		if (deadTimer > 0){
			deadTimer -= Time.deltaTime;
		}
		else{
			
			updateTimer = false;
			scoreTracker.GameEnded();
			
			gameScorePanel.SetActive(false);
			endPanel.SetActive(true);
			engineSound.StopAudio();
			ufoController.enabled = false;
			
			enabled = false;
		}
	}
}
