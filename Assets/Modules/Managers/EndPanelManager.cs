﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EndPanelManager : MonoBehaviour{

	public GameObject newHighScores;
	public NumberToImageCollection score;
	public NumberToImageCollection highScore;
	
	// Use this for initialization
	void Start (){

		score.SetCollection(PlayerPrefs.GetInt("Score"));
		highScore.SetCollection(PlayerPrefs.GetInt("HighScore"));

		if (PlayerPrefs.GetInt("NewHighScore") == 1){
			
			PlayerPrefs.SetInt("NewHighScore", 0);
			PlayerPrefs.Save();
			
			newHighScores.SetActive(true);
		}

	}

	public void Retry(){
		
		SceneManager.LoadScene("Game");
	}

	public void MainMenu(){
		
		SceneManager.LoadScene("MainMenu");
	}
}
