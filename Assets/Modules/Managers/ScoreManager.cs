﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour{

	public GameObject player;
	
	public float startingPosition;
	public float furthestPlayerPos;
	public int score;

	public Text scoreUI;

	public List<NumberToImage> number = new List<NumberToImage>();

	public NumberToImageCollection numbers;
	
	// Use this for initialization
	void Start (){

		startingPosition = player.transform.position.x;
	}
	
	// Update is called once per frame
	void Update (){

		if (player.transform.position.x > furthestPlayerPos)
			furthestPlayerPos = player.transform.position.x;
		
		score = Mathf.FloorToInt(furthestPlayerPos- startingPosition);
		//scoreUI.text = "Score: " + score.ToString();
		numbers.SetCollection(score);
	}

	public void GameEnded(){

		float lastScore = PlayerPrefs.GetInt("HighScore");

		PlayerPrefs.SetInt("Score", score);
		PlayerPrefs.Save();
		
		if (score > lastScore){
			
			PlayerPrefs.SetInt("HighScore", score);
			PlayerPrefs.SetInt("NewHighScore", 1);
			PlayerPrefs.Save();
		}
	}

	[ContextMenu("Reset scores")]
	void ResetAllScores(){
		
		PlayerPrefs.SetInt("Score", 0);
		PlayerPrefs.SetInt("HighScore", 0);
		PlayerPrefs.SetInt("NewHighScore", 0);
		PlayerPrefs.Save();
	}
}
