﻿using System.Collections.Generic;
using UnityEngine;

namespace LukeWaffel.TrackGenerator{
	
    public class TrackGenerator : MonoBehaviour{

	    [Header("References")]
	    public GameObject player;
	    
	    [Header("Config")]
	    public GameObject trackParent;
	    public PieceCollection collection;
	    [Space(10)]
	    public int spawnFuelEvery = 5;
	    public GameObject fuelPrefab;
	    
        TrackPiece previousPiece;
	    GameObject previousPieceObject;

	    float nextSpawnHorizontalPosition;
	    int piecesSpawned;
	
        // Use this for initialization
        void Start (){

            Initialize();
	        LayPieces(5);
        }

	    void Update(){

		    if (player.transform.position.x >= nextSpawnHorizontalPosition){
			    
			    
			    LayPieces(5);
		    }
	    }

        private void Initialize(){
		
            trackParent = new GameObject("Track");
		
        }

	    void LayPieces(int amount){

		    for (int i = 0; i < amount; i++){
			    
			    LayPiece();
			    
			    if(i == 0)
				    nextSpawnHorizontalPosition = previousPieceObject.transform.position.x;
		    }
		    
	    }
	    
        [ContextMenu("Lay pieces")]
        void LayPiece(){

	        TrackPiece pieceToLay = FindNextPiece(previousPiece);

            Vector3 spawnLocation = previousPiece ? previousPieceObject.transform.Find("End").position : Vector3.zero;

            Vector3 beginOffset = pieceToLay.prefab.transform.Find("Begin").localPosition;

            spawnLocation -= beginOffset;
		
            GameObject newPiece = Instantiate(pieceToLay.prefab, spawnLocation, Quaternion.identity);
            newPiece.transform.SetParent(trackParent.transform);

	        if (piecesSpawned % spawnFuelEvery == 0 && piecesSpawned > 0){

		        GameObject spawnedFuel = Instantiate(fuelPrefab, newPiece.transform.Find("Fuel").position, Quaternion.identity);
	        }
	        
            previousPiece = pieceToLay;
	        previousPieceObject = newPiece;

	        piecesSpawned++;
        }

	    TrackPiece FindNextPiece(TrackPiece previousPiece){
		    
		    TrackPiece offeredPiece = collection.pieces[Random.Range(0, collection.pieces.Count)];

		    if(offeredPiece.distanceRequired > 0)
			    if (player.transform.position.x < offeredPiece.distanceRequired)
				    return FindNextPiece(previousPiece);
		    
		    if (offeredPiece.requiredPrePiece.Count > 0){

			    if (offeredPiece.requiredPrePiece.Contains(previousPiece)){

				    return offeredPiece;
			    }else{

				    return FindNextPiece(previousPiece);
			    }
		    }

		    return offeredPiece;
	    }
    }
}