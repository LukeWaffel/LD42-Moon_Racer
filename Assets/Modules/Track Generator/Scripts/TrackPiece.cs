﻿using System.Collections.Generic;
using UnityEngine;

namespace LukeWaffel.TrackGenerator{

    [CreateAssetMenu(fileName = "New Track Piece", menuName = "LukeWaffel/Track Generator/New Track Piece", order = 1)]
    public class TrackPiece : ScriptableObject{

        public GameObject prefab;
        public List<TrackPiece> requiredPrePiece = new List<TrackPiece>();
        public float distanceRequired;
    }
}