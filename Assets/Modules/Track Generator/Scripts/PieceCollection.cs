﻿using System.Collections.Generic;
using UnityEngine;

namespace  LukeWaffel.TrackGenerator{

    [CreateAssetMenu(fileName = "New Collection", menuName = "LukeWaffel/Track Generator/New Collection", order = 0)]
    public class PieceCollection : ScriptableObject{

        public List<TrackPiece> pieces = new List<TrackPiece>();
    }    
}