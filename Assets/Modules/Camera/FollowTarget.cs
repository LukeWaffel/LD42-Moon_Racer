﻿using UnityEngine;

public class FollowTarget : MonoBehaviour{

	public Vector3 offset = new Vector3(0,0,-10);
	
	public enum Mode {Normal,Smooth};
	public Mode mode;

	public GameObject target;

	public bool decoupleOnStart;
	
	// Use this for initialization
	void Start () {
	
		if(decoupleOnStart)
			transform.SetParent(null);
	}
	
	// Update is called once per frame
	void Update () {

		if (mode == Mode.Normal){

			transform.position = target.transform.position + offset;
		}
	}

	[ContextMenu("Set Offset")]
	void SetOffset(){

		offset = transform.localPosition;
	}
}
